Big-O Running Times for Each of The Methods in MyJHUSet.java

MyJHUSet()
O(1) - constant

size()
O(1) - constant

isEmpty()
O(1) - constant

add()
N + N = 2N --> O(N) - linear

remove()
N + N-1 = 2N-1 --> O(N) - linear

contains()
N --> O(N) - linear

union()
N + N (for call to contains method) + 2N (for call to add method) = 4N --> O(N) - linear

intersect()
N + N (for call to contains method) + 2N-1 (for call to remove method) = 4N-1 --> O(N) - linear

toString()

N-1 --> O(N) - linear

ALTERNATE WAY OF IMPLEMENTING THE JHUSet INTERFACE
Intead of using an array, I could have used a singly linked list to implement the interface.
All of the overall running times of each method would be the same (e.g. constant, linear), however they may vary slightly. Linked lists and arrays are overall fairly similar.

The add method would still be linear however it would be slightly faster because I wouldn't have to create a second for loop in the method to create a new array with twice the length. In a linked list I can just add the element to the beginning of the linked list.

The union method would still be linear however it would be slightly faster because of the call to the add method (see above).