/**
 * @author Michael Ashmead
 * @param <T>
 *            can be any Object
 */
public class MyJHUSet<T> implements JHUSet<T> {
    /**
     * array starts with length 0.
     */
    private static final int START_SIZE = 0;
    
    /**
     * array for storing Ts.
     */
    private T[] array;

    /**
     * size keeps track of number of elements in array. (array length may be
     * longer than number of elements)
     */
    private int size;

    /**
     * 
     */
    public MyJHUSet() {
        // cast causes unavoidable warning below
        this.array = (T[]) new Object[MyJHUSet.START_SIZE];
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public boolean add(T o) { // finished
        // first check if the object we are trying to add
        // already exists in the array
        boolean objectExists = false;
        for (int x = 0; x < this.size; x++) {
            if (this.array[x].equals(o)) {
                objectExists = true;
                break;
            }
        }
        if (!objectExists) {
            if (this.size == this.array.length) {
                //cast causes unavoidable warning below
                T[] temp = (T[]) new Object[this.array.length * 2];
                for (int x = 0; x < this.size; x++) {
                    temp[x] = this.array[x];
                }
                this.array = temp;
            }
            // now we know there is room, so add o and update size
            this.array[this.size + 1] = o;
            this.size++;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Object o) { // finished
        boolean objectExists = false;
        int posOfObject = 0; // initialize to 0 however it won't be needed
                             // unless the objectExists (& in that case
                             // posOfObject will have been updated)
        for (int x = 0; x < this.size; x++) {
            if (this.array[x].equals(o)) {
                objectExists = true;
                posOfObject = x;
                break;
            }
        }
        if (objectExists) {
            T[] temp = (T[]) new Object[this.array.length]; // cast causes
                                                            // unavoidable
                                                            // warning
            for (int x = 0; x < posOfObject; x++) {
                temp[x] = this.array[x];
            }
            for (int x = posOfObject + 1; x < this.size; x++) {
                temp[x] = this.array[x];
            }
            this.array = temp;
            this.size--;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Object o) { // finished
        boolean objectExists = false;
        for (int x = 0; x < this.size; x++) {
            if (this.array[x].equals(o)) {
                objectExists = true;
                break;
            }
        }
        return objectExists;
    }

    @Override
    public JHUSet<T> union(JHUSet<T> that) { // finished
        JHUSet<T> temp = that;

        // loop through this set and add values that aren't already in temp to
        // temp
        for (int x = 0; x < this.size; x++) {
            if (!temp.contains(this.array[x])) {
                temp.add(this.array[x]);
            }
        }
        return temp;
    }

    @Override
    public JHUSet<T> intersect(JHUSet<T> that) { // finished
        JHUSet<T> temp = that;

        // loop through this set and remove values that are already in temp
        for (int x = 0; x < this.size; x++) {
            if (!temp.contains(this.array[x])) {
                temp.remove(this.array[x]);
            }
        }
        return temp;
    }

    @Override
    public String toString() { // finished
        if (this.size == 0) {
            return "{}";
        } else {
            String s = "{";
            for (int x = 0; x < this.size - 1; x++) {
                s = s + this.array[x] + ", ";
            }
            s = s + this.array[this.size - 1] + "}";
            return s;
        }
    }
}