import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class BoggleScorer {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String fileA = args[0];
        String fileB = args[1];
        String fileC = args[2];
        String fileD = args[2 + 1];

        MyJHUSet<String> setA = new MyJHUSet<String>();
        MyJHUSet<String> setB = new MyJHUSet<String>();
        MyJHUSet<String> setC = new MyJHUSet<String>();
        MyJHUSet<String> setD = new MyJHUSet<String>();

        BufferedReader br = new BufferedReader(new FileReader(new File(fileA)));
        String temp = br.readLine();
        while (temp != null) {
            String[] tempArray = temp.split("\\s+");
            for (int x = 0; x < tempArray.length; x++) {
                setA.add(tempArray[x].toLowerCase());
            }
            temp = br.readLine();
        }

        br = new BufferedReader(new FileReader(new File(fileB)));
        temp = br.readLine();
        while (temp != null) {
            String[] tempArray = temp.split("\\s+");
            for (int x = 0; x < tempArray.length; x++) {
                setB.add(tempArray[x].toLowerCase());
            }
            temp = br.readLine();
        }

        br = new BufferedReader(new FileReader(new File(fileA)));
        temp = br.readLine();
        while (temp != null) {
            String[] tempArray = temp.split("\\s+");
            for (int x = 0; x < tempArray.length; x++) {
                setC.add(tempArray[x].toLowerCase());
            }
            temp = br.readLine();
        }

        br = new BufferedReader(new FileReader(new File(fileA)));
        temp = br.readLine();
        while (temp != null) {
            String[] tempArray = temp.split("\\s+");
            for (int x = 0; x < tempArray.length; x++) {
                setD.add(tempArray[x].toLowerCase());
            }
            temp = br.readLine();
        }

        // remove words where word appears in 3 or more lists as specified in
        // Task 3
        // 4 unique combinations that are possible: A,B,C & A,C,D & A,B,D &
        // B,C,D
        String stringOfSetA = setA.toString();
        stringOfSetA = stringOfSetA.substring(1, stringOfSetA.length() - 1);
        String[] tempArrayA = stringOfSetA.split(", ");
        for (int x = 0; x < tempArrayA.length; x++) {
            if (setB.contains(tempArrayA[x]) && setC.contains(tempArrayA[x])) {
                // remove word from all sets
                setA.remove(tempArrayA[x]);
                setB.remove(tempArrayA[x]);
                setC.remove(tempArrayA[x]);
                setD.remove(tempArrayA[x]);
            } else if (setC.contains(tempArrayA[x])
                    && setD.contains(tempArrayA[x])) {
                // remove word from all sets
                setA.remove(tempArrayA[x]);
                setB.remove(tempArrayA[x]);
                setC.remove(tempArrayA[x]);
                setD.remove(tempArrayA[x]);
            } else if (setB.contains(tempArrayA[x])
                    && setD.contains(tempArrayA[x])) {
                // remove word from all sets
                setA.remove(tempArrayA[x]);
                setB.remove(tempArrayA[x]);
                setC.remove(tempArrayA[x]);
                setD.remove(tempArrayA[x]);
            }
        }
        String stringOfSetB = setB.toString();
        stringOfSetB = stringOfSetB.substring(1, stringOfSetB.length() - 1);
        String[] tempArrayB = stringOfSetB.split(", ");
        for (int x = 0; x < tempArrayB.length; x++) {
            if (setC.contains(tempArrayB[x]) && setD.contains(tempArrayB[x])) {
                // remove word from all sets
                setA.remove(tempArrayB[x]);
                setB.remove(tempArrayB[x]);
                setC.remove(tempArrayB[x]);
                setD.remove(tempArrayB[x]);
            }
        }

        int scoreA = getScore(setA);
        int scoreB = getScore(setB);
        int scoreC = getScore(setC);
        int scoreD = getScore(setD);

        // get highest score
        int highestScore = scoreA;
        if (scoreB > highestScore) {
            highestScore = scoreB;
        }
        if (scoreC > highestScore) {
            highestScore = scoreC;
        }
        if (scoreD > highestScore) {
            highestScore = scoreD;
        }

        // get sets that match highest score
        int numOfHighSets = 0;
        boolean[] arrayOfHighSets = {false, false, false, false};
        if (scoreA == highestScore) {
            arrayOfHighSets[0] = true;
            numOfHighSets++;
        }
        if (scoreB == highestScore) {
            arrayOfHighSets[1] = true;
            numOfHighSets++;
        }
        if (scoreC == highestScore) {
            arrayOfHighSets[2] = true;
            numOfHighSets++;
        }
        if (scoreD == highestScore) {
            arrayOfHighSets[2 + 1] = true;
            numOfHighSets++;
        }

        // print out all scores
        System.out.println("Score of Player A: " + scoreA);
        System.out.println("Score of Player B: " + scoreB);
        System.out.println("Score of Player C: " + scoreC);
        System.out.println("Score of Player D: " + scoreD);

        if (numOfHighSets == 1) {
            if (arrayOfHighSets[0]) {
                System.out.println("Player A is the winner!");
            } else if (arrayOfHighSets[1]) {
                System.out.println("Player B is the winner!");
            } else if (arrayOfHighSets[2]) {
                System.out.println("Player C is the winner!");
            } else {
                System.out.println("Player D is the winner!");
            }
        } else { // more than 1 player tied with the highest score in this case
            stringOfSetA = setA.toString();
            stringOfSetA = stringOfSetA.substring(1, stringOfSetA.length() - 1);
            tempArrayA = stringOfSetA.split(", ");

            stringOfSetB = setB.toString();
            stringOfSetB = stringOfSetB.substring(1, stringOfSetB.length() - 1);
            tempArrayB = stringOfSetB.split(", ");

            String stringOfSetC = setC.toString();
            stringOfSetC = stringOfSetC.substring(1, stringOfSetC.length() - 1);
            String[] tempArrayC = stringOfSetC.split(", ");

            String stringOfSetD = setD.toString();
            stringOfSetD = stringOfSetD.substring(1, stringOfSetD.length() - 1);
            String[] tempArrayD = stringOfSetD.split(", ");

            // sort 4 arrays above: tempArrayA, tempArrayB, tempArrayC, &
            // tempArrayD
            if (arrayOfHighSets[0]) {
                sort(tempArrayA);
            } else if (arrayOfHighSets[1]) {
                sort(tempArrayB);
            } else if (arrayOfHighSets[2]) {
                sort(tempArrayC);
            } else if (arrayOfHighSets[2 + 1]) {
                sort(tempArrayD);
            }

            int posCounter = 0;
            int currGWL = 0; //current greatest word length
            int setsWithCurrGreatestWordLength = 0;

            while (setsWithCurrGreatestWordLength != 1) {
                if (arrayOfHighSets[0]) {
                    if (tempArrayA[posCounter].length() > currGWL) {
                        currGWL = tempArrayA[posCounter]
                                .length();
                    }
                    if (tempArrayA[posCounter].length() == currGWL) {
                        setsWithCurrGreatestWordLength++;
                    } else {
                        arrayOfHighSets[0] = false;
                    }
                }
                if (arrayOfHighSets[1]) {
                    if (tempArrayB[posCounter].length() > currGWL) {
                        currGWL = tempArrayB[posCounter]
                                .length();
                    }
                    if (tempArrayB[posCounter].length() == currGWL) {
                        setsWithCurrGreatestWordLength++;
                    } else {
                        arrayOfHighSets[1] = false;
                    }
                }
                if (arrayOfHighSets[2]) {
                    if (tempArrayC[posCounter].length() > currGWL) {
                        currGWL = tempArrayC[posCounter]
                                .length();
                    }
                    if (tempArrayC[posCounter].length() == currGWL) {
                        setsWithCurrGreatestWordLength++;
                    } else {
                        arrayOfHighSets[2] = false;
                    }
                }
                if (arrayOfHighSets[2 + 1]) {
                    if (tempArrayD[posCounter].length() > currGWL) {
                        currGWL = tempArrayD[posCounter]
                                .length();
                    }
                    if (tempArrayD[posCounter].length() == currGWL) {
                        setsWithCurrGreatestWordLength++;
                    } else {
                        arrayOfHighSets[2 + 1] = false;
                    }
                }

                setsWithCurrGreatestWordLength = 0;
                posCounter++;
            }
            // print player that won
            if (arrayOfHighSets[0]) {
                System.out.println("Player A is the winner!");
            } else if (arrayOfHighSets[1]) {
                System.out.println("Player B is the winner!");
            } else if (arrayOfHighSets[2]) {
                System.out.println("Player C is the winner!");
            } else {
                System.out.println("Player D is the winner!");
            }
        }
    }
    
    /**
     * get score of specified set.
     * @param set of words
     * @return score of set
     */
    public static int getScore(MyJHUSet<String> set) {
        String stringOfSet = set.toString();
        stringOfSet = stringOfSet.substring(1, stringOfSet.length() - 1);
        String[] tempArray = stringOfSet.split(", ");

        int score = 0;
        for (int x = 0; x < tempArray.length; x++) {
            if (tempArray[x].length() == (2 + 1)
                || tempArray[x].length() == (2 + 2)) {
                score += 1;
            } else if (tempArray[x].length() == (2 + 2 + 1)) {
                score += 2;
            } else if (tempArray[x].length() == (2 + 2 + 2)) {
                score += (2 + 1);
            } else if (tempArray[x].length() == (2 + 2 + 2 + 1)) {
                score += (2 + 2 + 1);
            } else if (tempArray[x].length() >= (2 + 2 + 2 + 2)) {
                score += (2 + 2 + 2 + 2 + 2 + 1);
            }
        }
        return score;
    }

    /**
     * @param tempArray to sort
     */
    private static void sort(String[] tempArray) {
        boolean flag = true;
        String temp;

        while (flag) {
            flag = false; // set flag to false awaiting a possible swap
            for (int x = 0; x < tempArray.length - 1; x++) {
                if (tempArray[x].length() < tempArray[x + 1].length()) {
                    temp = tempArray[x];
                    tempArray[x] = tempArray[x + 1];
                    tempArray[x + 1] = temp;
                    flag = true;
                }
            }
        }
    }
}